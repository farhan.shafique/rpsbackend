#include <string>
#include <eosiolib/eosio.hpp>
#include <eosiolib/time.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/transaction.hpp>
#include <eosiolib/crypto.h>

using eosio::action;
using eosio::asset;
using eosio::name;
using eosio::permission_level;
using eosio::print;
using eosio::symbol_code;
using eosio::time_point_sec;
using eosio::transaction;
using eosio::unpack_action_data;

namespace rps_project
{

CONTRACT rps : public eosio::contract
{
	using contract::contract;

  public:
	rps(name receiver, name code, eosio::datastream<const char *> ds) : eosio::contract(receiver, code, ds),
																		bets(_self, _self.value),
																		games(_self, _self.value),
																		freezee(_self, _self.value),
																		ref(_self, _self.value){};

  public:
	const uint32_t TWO_MINUTES = 0 * 60;
	const uint32_t TEN_MINUTES = 0 * 60;

	[[eosio::action]] void freezegame(uint32_t status);
	[[eosio::action]] void cancelgame(const uint64_t game_id, name &by);
	[[eosio::action]] void creategame(uint64_t gameId, name host, name challenger, asset bet);
	[[eosio::action]] void joingame(uint64_t gameId, name challenger);
	[[eosio::action]] void transfer(uint64_t sender, uint64_t receiver);
	[[eosio::action]] void commithash(uint64_t gameid, name &by, capi_checksum256 move_hash);
	[[eosio::action]] void revealchoice(uint64_t gameid, const name &by, uint32_t &pmove, uint32_t &pmove_nonce);
	[[eosio::action]] void gameend(uint64_t gameid, const name &winner, const uint32_t &ph_move, const uint32_t &pc_move);
	[[eosio::action]] void maintenance(name by);
	[[eosio::action]] void winner(const uint64_t gameid, const time_point_sec &hostDate, const time_point_sec &opponentDate, const name &host, const name &opponent, const uint32_t &hostChoice, const uint32_t &opponentChoice, const uint32_t &betAmounnt, std::string &gameType, const name &gameResult, const name &hostReferrel, const name &opponentReferrel);
	[[eosio::action]] void practice(uint64_t id);
	void token_assert(asset bet);
	name get_referral(name account);
	void report(const uint64_t gameid, const time_point_sec &hostDate, const time_point_sec opponentDate, const name &host, const name &opponent, const uint32_t &hostChoice, const uint32_t &opponentChoice, const uint32_t &betAmount, std::string gameType, const name &gameResult, const name &hostReferrel, const name &opponentReferrel);
	void payout(name to, name referral, uint32_t amount, name winner, std::string memo);

  private:
	struct [[eosio::table]] bet
	{
		uint64_t gameid;
		name host;
		name challenger;
		uint64_t bet_amt;
		time_point_sec host_time;
		time_point_sec challenger_time;

		uint64_t primary_key() const { return gameid; }

		EOSLIB_SERIALIZE(bet, (gameid)(host)(challenger)(bet_amt)(host_time)(challenger_time))
	};

	struct [[eosio::table]] game
	{
		uint64_t gameid;
		std::string gametype;
		name host;
		name challenger;
		asset bet;
		time_point_sec game_time;
		capi_checksum256 ph_move_hash;
		uint32_t ph_move;
		uint32_t ph_move_nonce;
		capi_checksum256 pc_move_hash;
		uint32_t pc_move;
		uint32_t pc_move_nonce;
		name winner = "none"_n;
		uint32_t gameStart = 0;

		uint64_t primary_key() const { return gameid; }

		EOSLIB_SERIALIZE(game, (gameid)(gametype)(host)(challenger)(bet)(game_time)(ph_move_hash)(ph_move)(ph_move_nonce)(pc_move_hash)(pc_move)(pc_move_nonce)(winner)(gameStart))
	};

	struct [[eosio::table]] referral
	{
		name account;
		name ref_ac;

		uint64_t primary_key() const { return account.value; }

		EOSLIB_SERIALIZE(referral, (account)(ref_ac))
	};

	struct [[eosio::table]] freeze
	{
		uint64_t id;
		uint32_t status = 0;

		uint64_t primary_key() const { return id; }

		EOSLIB_SERIALIZE(freeze, (id)(status))
	};

	struct st_transfer
	{
		name from;
		name to;
		asset quantity;
		std::string memo;
	};

	struct [[eosio::table]] account
	{
		asset balance;

		uint64_t primary_key() const { return balance.symbol.code().raw(); }
	};

	typedef eosio::multi_index<"accounts"_n, account> accounts;

	// struct st_seeds
	// {
	// 	capi_checksum256 seed1;
	// 	capi_checksum256 seed2;
	// };

	typedef eosio::multi_index<"bet"_n, bet>
		bet_index;

	typedef eosio::multi_index<"game"_n, game>
		game_index;

	typedef eosio::multi_index<"referral"_n, referral>
		referral_index;

	typedef eosio::multi_index<"freeze"_n, freeze>
		freeze_index;

  public:
	bet_index bets;
	game_index games;
	freeze_index freezee;
	referral_index ref;
};

} // namespace rps_project
