#include "rps.hpp"

using namespace rps_project;

[[eosio::action]] void rps_project::rps::freezegame(uint32_t status) {
	require_auth(_self);

	auto itr = freezee.begin();

	if (itr == freezee.end())
	{
		freezee.emplace(_self, [&](auto &g) {
			g.status = status;
		});
	}
	else
	{
		freezee.modify(itr, _self, [&](auto &g) {
			g.status = status;
		});
	}
};

[[eosio::action]] void rps_project::rps::cancelgame(const uint64_t game_id, name &by) {
	require_auth(by);

	auto itr = bets.find(game_id);
	auto game_itr = games.find(game_id);

	eosio_assert(game_itr != games.end(), "Game doesn't exist");
	eosio_assert(game_itr->host == by || game_itr->challenger == by, "This is not your game");

	if (itr != bets.end())
	{

		time_point_sec bet_time;

		name referral = "none"_n;

		name host_referral = get_referral(itr->host);
		name challenger_referral = get_referral(itr->challenger);

		if (itr->host == by)
			bet_time = itr->host_time;
		else
			bet_time = itr->challenger_time;

		if (game_itr->pc_move == 0 && game_itr->ph_move == 0)
		{

			if (itr->host != "none"_n)
				payout(itr->host, referral, itr->bet_amt, "none"_n, "Host Refund back - Cancel game");

			if (itr->challenger != "none"_n && itr->challenger != _self)
				payout(itr->challenger, referral, itr->bet_amt, "none"_n, "Challenger Refund back - Cancel game");
		}
		else if (game_itr->pc_move == 0 && game_itr->ph_move != 0)
		{
			eosio_assert(time_point_sec(now() - TWO_MINUTES) > bet_time, "Wait 2 minutes");
			payout(itr->host, host_referral, itr->bet_amt, itr->host, "Host winning amount");
		}
		else if (game_itr->pc_move != 0 && game_itr->ph_move == 0 && itr->challenger != _self)
		{
			eosio_assert(time_point_sec(now() - TWO_MINUTES) > bet_time, "Wait 2 minutes");
			payout(itr->challenger, challenger_referral, itr->bet_amt, itr->challenger, "Challenger winning amount");
		}

		bets.erase(itr++);
	}

	games.erase(game_itr++);
};

[[eosio::action]] void rps_project::rps::creategame(uint64_t gameId, name host, name challenger, asset bet) {
	require_auth(host);

	auto game_itr = games.find(gameId);
	auto itr = bets.find(gameId);

	name host_referral = get_referral(itr->host);
	name challenger_referral = get_referral(itr->challenger);

	eosio_assert(game_itr == games.end(), "game already exist");
	eosio_assert(itr == bets.end(), "game already exist");

	token_assert(bet);

	games.emplace(_self, [&](auto &g) {
		g.gameid = gameId;
		g.gametype = "PVP";
		g.host = host;

		if (is_account(challenger) && challenger != host)
		{
			g.challenger = challenger;
		}
		else
			g.challenger = "none"_n;

		g.game_time = time_point_sec(now());
		g.ph_move = 0;
		g.ph_move_nonce = 0;
		g.pc_move = 0;
		g.pc_move_nonce = 0;
		g.winner = "none"_n;
		g.bet = bet;
	});

	name referral = "none"_n;

	game_itr = games.begin();
	itr = bets.begin();

	while (itr != bets.end())
	{
		game_itr = games.find(itr->gameid);

		if (time_point_sec(now() - TEN_MINUTES) > game_itr->game_time)
		{

			if (game_itr != games.end())
			{

				if (game_itr->pc_move == 0 && game_itr->ph_move == 0)
				{

					if (itr->host != "none"_n)
						payout(itr->host, referral, itr->bet_amt, "none"_n, "Host Refund Back - Expire game");

					if (itr->challenger != "none"_n && itr->challenger != _self)
						payout(itr->challenger, referral, itr->bet_amt, "none"_n, "Challenger Refund Back - Expire game");
				}
				else if (game_itr->pc_move == 0 && game_itr->ph_move != 0)
				{
					payout(itr->host, host_referral, itr->bet_amt, itr->host, "Host winning amount");
				}
				else if (game_itr->pc_move != 0 && game_itr->ph_move == 0 && game_itr->challenger != _self)
				{
					payout(itr->challenger, challenger_referral, itr->bet_amt, itr->challenger, "Challenger winning amount");
				}
			}

			bets.erase(itr++);
		}
		else
			itr++;
	}

	game_itr = games.begin();

	while (game_itr != games.end())
	{
		if (time_point_sec(now() - TEN_MINUTES) > game_itr->game_time)
		{

			games.erase(game_itr++);
		}
		else
			game_itr++;
	}
};

[[eosio::action]] void rps_project::rps::joingame(uint64_t gameId, name challenger) {
	require_auth(challenger);

	auto game_itr = games.find(gameId);

	eosio_assert(game_itr != games.end(), "game doesn't exist");

	eosio_assert(challenger != game_itr->host, "Host cannot be Challenger");

	eosio_assert(game_itr->challenger == "none"_n, "Challenger already join the game");

	games.modify(game_itr, _self, [&](auto &g) {
		g.challenger = challenger;
	});
};

[[eosio::action]] void rps_project::rps::transfer(uint64_t sender, uint64_t receiver) {
	auto transfer_data = unpack_action_data<st_transfer>();

	if (transfer_data.from == _self || transfer_data.from == "safeaccounts"_n || transfer_data.to != _self)
	{
		return;
	}

	token_assert(transfer_data.quantity);

	eosio_assert(transfer_data.quantity.is_valid(), "Invalid asset");
	auto sym = asset(0, eosio::symbol("EOS", 4));
	eosio_assert(transfer_data.quantity.symbol == sym.symbol, "We only accept EOS token");
	eosio_assert(transfer_data.quantity.amount >= 5000, "transfer quantity must be greater than 0.5");

	std::string game_id_str = NULL, ref_str = NULL, seed_str = NULL, game_str = NULL;
	const uint64_t your_bet_amount = (uint64_t)transfer_data.quantity.amount;

	const std::size_t first_break = transfer_data.memo.find("-");
	if (first_break != std::string::npos)
	{

		game_id_str = transfer_data.memo.substr(0, first_break);
		const std::string after_first_break = transfer_data.memo.substr(first_break + 1);
		const std::size_t second_break = after_first_break.find("-");

		if (second_break != std::string::npos)
		{

			ref_str = after_first_break.substr(0, second_break);
			const std::string after_second_break = after_first_break.substr(second_break + 1);
			const std::size_t third_break = after_second_break.find("-");

			if (third_break != std::string::npos)
			{

				seed_str = after_second_break.substr(0, third_break);
				game_str = after_second_break.substr(third_break + 1);
			}
		}
	}

	const uint64_t game_id = std::strtoul(game_id_str.c_str(), NULL, 10);

	const name possible_ref = name{ref_str.c_str()};

	// const uint64_t seed = eosio::string_to_name(seed_str.c_str());

	eosio_assert(game_id != 0 && seed_str != "" && game_str != "", "Invalid Bet");

	name referral = "none"_n;

	if (possible_ref != _self && possible_ref != transfer_data.from && is_account(possible_ref))
	{
		referral = possible_ref;
	}

	/* Auto Generate Game Id
		capi_checksum256 user_seed_hash;
		sha256((char *)&seed_str, seed_str.length(), &user_seed_hash);
		auto s = read_transaction(nullptr, 0);
		char *tx = (char *)malloc(s);
		read_transaction(tx, s);

		capi_checksum256 tx_hash;
		sha256(tx, s, &tx_hash);

		st_seeds seeds;
		seeds.seed1 = user_seed_hash;
		seeds.seed2 = tx_hash;

		capi_checksum256 seed_hash;
		sha256((char *)&seeds.seed1, sizeof(seeds.seed1) * 2, &seed_hash);
		const uint64_t game_id = ((uint64_t)tx_hash.hash[0] << 56) + ((uint64_t)tx_hash.hash[1] << 48) + ((uint64_t)tx_hash.hash[2] << 40) + ((uint64_t)tx_hash.hash[3] << 32) + ((uint64_t)tx_hash.hash[4] << 24) + ((uint64_t)tx_hash.hash[5] << 16) + ((uint64_t)tx_hash.hash[6] << 8) + (uint64_t)tx_hash.hash[7]; */

	auto itr = bets.find(game_id);
	auto game_itr = games.find(game_id);

	if (game_str != "PVC")
	{

		eosio_assert(game_itr != games.end(), "Game doesn't exist");

		eosio_assert(transfer_data.from == game_itr->host || transfer_data.from == game_itr->challenger, "This is not your game");

		eosio_assert(game_itr->challenger != "none"_n, "Wait for Opponent to join the Game");

		eosio_assert(game_itr->bet.amount == your_bet_amount, "Invalid Bet Amount");

		if (itr == bets.end())
		{

			bets.emplace(_self, [&](auto &k) {
				k.gameid = game_id;

				if (transfer_data.from == game_itr->host)
				{
					k.host = transfer_data.from;
					k.host_time = time_point_sec(now());
					k.challenger = "none"_n;
				}
				else if (transfer_data.from == game_itr->challenger)
				{
					k.challenger = transfer_data.from;
					k.challenger_time = time_point_sec(now());
					k.host = "none"_n;
				}

				k.bet_amt = your_bet_amount;
			});
		}
		else
		{
			eosio_assert(transfer_data.from != itr->host && transfer_data.from != itr->challenger, "You already bet on this game");

			bets.modify(itr, _self, [&](auto &g) {
				if (transfer_data.from == game_itr->host)
				{
					g.host = transfer_data.from;
					g.host_time = time_point_sec(now());
				}

				if (transfer_data.from == game_itr->challenger)
				{
					g.challenger = transfer_data.from;
					g.challenger_time = time_point_sec(now());
				}
			});

			games.modify(game_itr, _self, [&](auto &g) {
				g.gameStart = 1;
			});
		}
	}
	else
	{

		eosio_assert(itr == bets.end(), "game already exist");

		eosio_assert(game_id != game_itr->gameid, "game already created with this ID");

		bets.emplace(_self, [&](auto &k) {
			k.gameid = game_id;
			k.host = transfer_data.from;
			k.host_time = time_point_sec(now());
			k.challenger = _self;
			k.bet_amt = transfer_data.quantity.amount;
		});

		games.emplace(_self, [&](auto &g) {
			g.gameid = game_id;
			g.gametype = "PVC";
			g.host = transfer_data.from;
			g.challenger = _self;
			g.game_time = time_point_sec(now());
			g.ph_move = 0;
			g.ph_move_nonce = 0;
			g.pc_move = 0;
			g.pc_move_nonce = 0;
			g.winner = "none"_n;
			g.bet = transfer_data.quantity;
		});
	}

	auto ref_itr = ref.find(transfer_data.from.value);

	if (ref_itr == ref.end())
	{
		if (referral != "none"_n)
		{
			ref.emplace(_self, [&](auto &k) {
				k.account = transfer_data.from;
				k.ref_ac = referral;
			});
		}
	}
};

[[eosio::action]] void rps_project::rps::commithash(uint64_t gameid, name &by, capi_checksum256 move_hash) {
	require_auth(by);

	auto game_itr = games.find(gameid);
	auto itr = bets.find(gameid);

	eosio_assert(game_itr != games.end(), "game doesn't exists");

	eosio_assert(by == game_itr->host || by == game_itr->challenger, "this is not your game!");

	// if (game_itr->challenger == _self && game_itr->pc_move_hash.hash[0] == 0)
	// {
	// 	by = game_itr->challenger;
	// }
	// else if (game_itr->pc_move_hash.hash[0] != 0)
	// {
	// 	by = game_itr->host;
	// }

	if (by == game_itr->host)
		eosio_assert(by == itr->host, "Please place the bet first (Host)");
	else
		eosio_assert(by == itr->challenger, "Please place the bet first (Challenger)");

	eosio_assert(move_hash.hash[0] != 0, "Hash Shouldn't of Zero Bit");

	if (by == itr->host)
	{
		eosio_assert(game_itr->ph_move_hash.hash[0] == 0, "You cannot change hash Again");
	}
	else
	{
		eosio_assert(game_itr->pc_move_hash.hash[0] == 0, "You cannot change hash Again");
	}

	games.modify(game_itr, _self, [&](auto &g) {
		if (by == itr->host)
		{
			g.ph_move_hash = move_hash;

			bets.modify(itr, _self, [&](auto &g) {
				g.host_time = time_point_sec(now());
			});
		}
		else
		{
			g.pc_move_hash = move_hash;

			bets.modify(itr, _self, [&](auto &g) {
				g.host_time = time_point_sec(now());
			});
		}
		g.game_time = time_point_sec(now());
	});
};

[[eosio::action]] void rps_project::rps::revealchoice(uint64_t gameid, const name &by, uint32_t &pmove, uint32_t &pmove_nonce) {
	require_auth(by);

	auto game_itr = games.find(gameid);
	auto itr = bets.find(gameid);

	eosio_assert(game_itr != games.end(), "game doesn't exists");

	eosio_assert(by == game_itr->host || by == game_itr->challenger, "this is not your game!");

	eosio_assert((pmove <= 3 && pmove > 0) && (pmove_nonce > 0), "Invalid Move");

	if (by == game_itr->host)
	{
		eosio_assert((game_itr->ph_move_hash.hash[0] != 0), "Please reveal your hash first");
		eosio_assert((game_itr->pc_move_hash.hash[0] != 0), "Please wait for opponent to reveal the hash");
	}
	else
	{

		eosio_assert((game_itr->pc_move_hash.hash[0] != 0), "Please reveal your hash first");
		eosio_assert((game_itr->ph_move_hash.hash[0] != 0), "Please wait for opponent to reveal the hash");
	}

	// capi_checksum256 result;

	std::string move_check = std::to_string(pmove) + std::to_string(pmove_nonce);

	// sha256((char *)&move_check[0], move_check.size(), &result);

	if (by == itr->host)
		// eosio_assert(result == game_itr->ph_move_hash, "Move not confirmed!");
		assert_sha256(&move_check[0], move_check.length(), &game_itr->ph_move_hash);
	else
		assert_sha256(&move_check[0], move_check.length(), &game_itr->pc_move_hash);
	// eosio_assert(result == game_itr->pc_move_hash, "Move not confirmed!");

	games.modify(game_itr, _self, [&](auto &g) {
		if (by == itr->host)
		{
			g.ph_move = pmove;
			g.ph_move_nonce = pmove_nonce;
			g.game_time = time_point_sec(now());

			bets.modify(itr, _self, [&](auto &g) {
				g.host_time = time_point_sec(now());
			});

			if (g.pc_move != 0)
			{
				g.winner = "none"_n;

				if ((g.ph_move == 1 && g.pc_move == 3) || (g.ph_move == 2 && g.pc_move == 1) || (g.ph_move == 3 && g.pc_move == 2))
				{
					g.winner = itr->host;
				}

				if ((g.pc_move == 1 && g.ph_move == 3) || (g.pc_move == 2 && g.ph_move == 1) || (g.pc_move == 3 && g.ph_move == 2))
				{
					g.winner = itr->challenger;
				}

				gameend(gameid, g.winner, g.ph_move, g.pc_move);
			}
		}
		else
		{
			g.pc_move = pmove;
			g.pc_move_nonce = pmove_nonce;
			g.game_time = time_point_sec(now());

			if (g.ph_move != 0)
			{
				g.winner = "none"_n;

				if ((g.ph_move == 1 && g.pc_move == 3) || (g.ph_move == 2 && g.pc_move == 1) || (g.ph_move == 3 && g.pc_move == 2))
				{
					g.winner = itr->host;
				}
				if ((g.pc_move == 1 && g.ph_move == 3) || (g.pc_move == 2 && g.ph_move == 1) || (g.pc_move == 3 && g.ph_move == 2))
				{
					g.winner = itr->challenger;
				}

				gameend(gameid, g.winner, g.ph_move, g.pc_move);
			}
		}
	});

	if (game_itr->ph_move != 0 && game_itr->pc_move != 0)
	{

		games.erase(game_itr);
		bets.erase(itr);
	}
};

void rps_project::rps::gameend(uint64_t gameid, const name &winner, const uint32_t &ph_move, const uint32_t &pc_move)
{
	auto itr = bets.find(gameid);
	auto game_itr = games.find(gameid);

	name host_referral = get_referral(itr->host);
	name challenger_referral = get_referral(itr->challenger);

	if (winner == itr->host)
	{
		payout(itr->host, host_referral, itr->bet_amt, winner, "Host winning amount");
	}
	else if (winner == itr->challenger)
	{
		if (itr->challenger != _self)
			payout(itr->challenger, challenger_referral, itr->bet_amt, winner, "Challenger winning amount");
	}
	else
	{
		payout(itr->host, host_referral, itr->bet_amt, "none"_n, "Host Refund Back - Game Tie");

		if (itr->challenger != _self)
			payout(itr->challenger, challenger_referral, itr->bet_amt, "none"_n, "Challenger Refund Back - Game Tie");
	}

	report(gameid, itr->host_time, itr->challenger_time, itr->host, itr->challenger, game_itr->ph_move, game_itr->pc_move, itr->bet_amt, game_itr->gametype, winner, host_referral, challenger_referral);
};

[[eosio::action]] void rps_project::rps::winner(const uint64_t gameid, const time_point_sec &hostDate, const time_point_sec &opponentDate, const name &host, const name &opponent, const uint32_t &hostChoice, const uint32_t &opponentChoice, const uint32_t &betAmounnt, std::string &gameType, const name &gameResult, const name &hostReferrel, const name &opponentReferrel) {
};

[[eosio::action]] void rps_project::rps::maintenance(name by) {
	require_auth(by);

	eosio_assert(by == _self, "You're not authorized to execute this function");

	auto game_itr = games.begin();
	auto itr = bets.begin();

	name referral = "none"_n;

	while (itr != bets.end())
	{

		if (itr->host != "none"_n)
			payout(itr->host, referral, itr->bet_amt, "none"_n, "Host Refund Back - Game is on Maintenance");

		if (itr->challenger != "none"_n)
			payout(itr->challenger, referral, itr->bet_amt, "none"_n, "Challenger Refund Back - Game is on Maintenance");

		bets.erase(itr++);
	}

	while (game_itr != games.end())
	{
		games.erase(game_itr++);
	}
};

void rps_project::rps::token_assert(asset bet)
{

	eosio_assert(bet.is_valid(), "Invalid asset");
	auto sym = asset(0, eosio::symbol("EOS", 4));
	eosio_assert(bet.symbol == sym.symbol, "We only accept EOS token");
	eosio_assert(bet.amount >= 5000, "transfer quantity must be greater than 0.5");

	accounts acct("eosio.token"_n, _self.value);

	auto itr = acct.find(sym.symbol.code().raw());

	int account_balance = (int)itr->balance.amount;

	int limit = (int)account_balance * 0.05;

	eosio_assert(bet.amount < limit, "You have reached maximum bet limit");
};

name rps_project::rps::get_referral(name account)
{
	auto itr = ref.find(account.value);

	name referral;

	if (itr != ref.end())
		referral = itr->ref_ac;
	else
		referral = "none"_n;

	return referral;
};

void rps_project::rps::report(const uint64_t gameid, const time_point_sec &hostDate, const time_point_sec opponentDate, const name &host, const name &opponent, const uint32_t &hostChoice, const uint32_t &opponentChoice, const uint32_t &betAmount, std::string gameType, const name &gameResult, const name &hostReferrel, const name &opponentReferrel)
{
	action(permission_level{_self, "active"_n},
		   _self,
		   "winner"_n,
		   std::make_tuple(
			   gameid, hostDate, opponentDate, host, opponent, hostChoice, opponentChoice, betAmount, gameType, gameResult, hostReferrel, opponentReferrel))
		.send();
};

void rps_project::rps::payout(name to, name referral, uint32_t amount, name winner, std::string memo)
{

	if (winner == "none"_n)
	{
		action(
			permission_level{_self, "active"_n},
			"eosio.token"_n,
			"transfer"_n,
			std::make_tuple(
				_self,
				to,
				asset(amount, eosio::symbol("EOS", 4)), std::string(memo)))
			.send();
	}
	else
	{
		uint32_t fee = amount * (1.5 / 100);
		uint32_t winning_amount = (amount * 2) - fee;
		uint32_t ref_amount = fee * (25 / 100);

		action(
			permission_level{_self, "active"_n},
			"eosio.token"_n,
			"transfer"_n,
			std::make_tuple(
				_self,
				to,
				asset(winning_amount, eosio::symbol("EOS", 4)), std::string(memo)))
			.send();

		if (ref_amount > 0 && referral != "none"_n)
		{
			action(
				permission_level{_self, "active"_n},
				"eosio.token"_n,
				"transfer"_n,
				std::make_tuple(
					_self,
					referral,
					asset(ref_amount, eosio::symbol("EOS", 4)), std::string("Referrel Amount")))
				.send();
		}
	}
};

void frozen(uint64_t receiver, uint64_t code)
{
	std::string const hello = "hello";
	auto data = eosio::datastream(&hello[0], hello.length());
	rps_project::rps c(name{receiver}, name{code}, data);
	auto itr = c.freezee.begin();
	if (itr != c.freezee.end())
		eosio_assert(itr->status == 0, "Contract has been freeze");
};

#define EOSIO_DISPATCH_EX(TYPE, MEMBERS)                                                              \
	extern "C"                                                                                        \
	{                                                                                                 \
		void apply(uint64_t receiver, uint64_t code, uint64_t action)                                 \
		{                                                                                             \
			if (action != name("freezegame").value && action != name("maintenance").value)            \
			{                                                                                         \
				frozen(receiver, code);                                                               \
			}                                                                                         \
			auto self = receiver;                                                                     \
			if (code == self || code == name("eosio.token").value)                                    \
			{                                                                                         \
				if (action == name("frozen").value)                                                   \
				{                                                                                     \
					eosio_assert(code == name("eosiorpsgame").value, "Cannot Trigger Frozen Action"); \
				}                                                                                     \
				if (action == name("winner").value)                                                   \
				{                                                                                     \
					eosio_assert(code == name("eosiorpsgame").value, "Action Unavailable");           \
				}                                                                                     \
				if (action == name("transfer").value)                                                 \
				{                                                                                     \
					eosio_assert(code == name("eosio.token").value, "Must transfer EOS");             \
				}                                                                                     \
				switch (action)                                                                       \
				{                                                                                     \
					EOSIO_DISPATCH_HELPER(TYPE, MEMBERS)                                              \
				}                                                                                     \
				/* does not allow destructor of thiscontract to run: eosio_exit(0); */                \
			}                                                                                         \
		}                                                                                             \
	}

EOSIO_DISPATCH_EX(rps, (creategame)(joingame)(transfer)(commithash)(revealchoice)(freezegame)(cancelgame)(winner)(maintenance))
